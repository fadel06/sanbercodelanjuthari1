<?php

trait Fight{
  //property
  public $attackPower;
  public $defencePower;

  public function serang($penyerang, $diserang){
    echo $penyerang->nama_hewan." sedang menyerang " . $diserang->nama_hewan . '<br>';
    $diserang->diserang($penyerang, $diserang);
  }
  public function diserang($penyerang, $diserang){
    echo $diserang->nama_hewan. " sedang diserang ". $penyerang->nama_hewan. '<br>';
    $darah_sekarang = $diserang->darah - $penyerang->attackPower / $diserang->defencePower;
    echo $diserang->nama_hewan . ' darah sekarang ' . $darah_sekarang . '<br><br>' ;
  }
}

trait Hewan
{
  public $nama_hewan;
  public $darah = 50;
  public $jumlah_kaki;
  public $keahlian;

  public function atraksi(){
    echo $this->nama_hewan . "sedang" . $this->keahlian . '<br>';
  }
}

class Elang
{
    use Fight;
    use Hewan;

    function __construct($nama_hewan){
      $this->nama_hewan = $nama_hewan;
      $this->jumlah_kaki = 2;
      $this->keahlian = 'terbang tinggi';
      $this->attackPower = 10;
      $this->defencePower = 5;
    }

    public function getInfoHewanElang(){
      echo 'Nama Hewan : ' . $this->nama_hewan . '<br>';
      echo 'Jumlah Kaki : ' . $this->jumlah_kaki . '<br>';
      echo 'Keahlian : ' . $this->keahlian . '<br>';
      echo 'Attack Power : ' . $this->attackPower . '<br>';
      echo 'Defence Power : ' . $this->defencePower . '<br><br>';
    }
}

class Harimau
{
    use Fight;
    use Hewan;

    function __construct($nama_hewan){
      $this->nama_hewan = $nama_hewan;
      $this->jumlah_kaki = 4;
      $this->keahlian = 'lari cepat';
      $this->attackPower = 7;
      $this->defencePower = 8;
    }

    public function getInfoHewanHarimau(){
      echo 'Nama Hewan : ' . $this->nama_hewan . '<br>';
      echo 'Jumlah Kaki : ' . $this->jumlah_kaki . '<br>';
      echo 'Keahlian : ' . $this->keahlian . '<br>';
      echo 'Attack Power : ' . $this->attackPower . '<br>';
      echo 'Defence Power : ' . $this->defencePower . '<br><br>';
    }
}

$elang3 = new Elang('Elang_3');
$harimau1 = new Harimau('Harimau_1');

$harimau1->serang($harimau1, $elang3);
$elang3-> serang($elang3, $harimau1);

$elang3->getInfoHewanElang();
$harimau1->getInfoHewanHarimau();
?>
